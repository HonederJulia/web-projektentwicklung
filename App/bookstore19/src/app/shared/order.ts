import {Book} from "./book";
import {User} from "./user";
import {Status} from "./status";

export class Order {
    constructor(public id: number, public user_id: number, public billing_address_id: number,
                public shipping_address_id: number,  public date: Date, public price_total_net: number,
                public price_total_gross: number, public books?: Book[], public user?: User,
                public statuses?: Status[]) {
    }
}