import {Injectable} from '@angular/core';
import {Observable, throwError} from "rxjs/index";
import {catchError, retry} from "rxjs/internal/operators";
import {Order} from "./order";
import {HttpClient} from "@angular/common/http";
import {Address} from "./address";
import {Status} from "./status";

@Injectable({
    providedIn: 'root'
})
export class OrderService {

    private api = "http://bookstore19.s1610456012.student.kwmhgb.at/api";

    constructor(private http: HttpClient) {
    }

    getAll(): Observable<Array<Order>> {
        return this.http.get(`${this.api}/myorders`).pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }

    getAllByUserId(userId): Observable<Array<Order>> {
        return this.http.get(`${this.api}/myorders/${userId}`).pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }

    getSingle(id): Observable<Order> {
        return this.http.get(`${this.api}/myorder/${id}`).pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }

    getAddresses(userId): Observable<Array<Address>> {
        return this.http.get(`${this.api}/addresses/${userId}`).pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }

    saveOrder(order: Order): Observable<any> {
        return this.http.post(`${this.api}/myorder`, order).pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }

    saveStatus(status: Status): Observable<any> {
        return this.http.post(`${this.api}/status`, status).pipe(retry(3))
            .pipe(catchError(this.errorHandler));
    }

    private errorHandler(error: Error | any): Observable<any> {
        return throwError(error);
    }
}