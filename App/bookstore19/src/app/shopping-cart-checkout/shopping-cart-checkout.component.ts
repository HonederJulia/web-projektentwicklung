import {Component, OnInit} from '@angular/core';
import {AuthService} from "../shared/authentication.service";
import {OrderService} from "../shared/order.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Order} from "../shared/order";
import {formatCurrency} from "@angular/common";

@Component({
    selector: 'bs-shopping-cart-checkout',
    templateUrl: './shopping-cart-checkout.component.html',
    styles: []
})
export class ShoppingCartCheckoutComponent implements OnInit {

    books = [];
    billing_address;
    shipping_address;
    sum_gross = 0;

    constructor(public os: OrderService,
                public authService: AuthService,
                private route: ActivatedRoute,
                private router: Router) {
    }

    ngOnInit() {
        for (let i = 0; i < localStorage.length; i++) {
            let key = localStorage.key(i);
            if (key.slice(0, 5) === 'book_') {
                let book = JSON.parse(localStorage.getItem(key));
                this.sum_gross += book.price_net * 1.1 * +this.getAmount(book);
                this.books.push(book);
            }
        }

        this.billing_address = this.getBillingAddress();
        this.shipping_address = this.getShippingAddress();
    }

    getAmount(book) {
        return localStorage.getItem('amount_book_' + book.isbn);
    }

    getBillingAddress() {
        return JSON.parse(localStorage.getItem('billing_address'));
    }

    getShippingAddress(){
        return JSON.parse(localStorage.getItem('shipping_address'));
    }

    saveOrder() {
        let order = new Order(null, +localStorage.getItem('userId'), this.getBillingAddress().id,
                                this.getShippingAddress().id, new Date(), (this.sum_gross / 1.1), this.sum_gross);

        let books = [];
        for (let i = 0; i < localStorage.length; i++) {
            let key = localStorage.key(i);
            if (key.slice(0, 5) === 'book_') {
                let book = JSON.parse(localStorage.getItem(key));
                book.amount = +this.getAmount(book);
                books.push(book);
            }
        }

        order.books = books;

        this.os.saveOrder(order).subscribe(res => {
            this.router.navigate(['../myorders'], {relativeTo: this.route});
        });

        this.resetLocalStorage();
    }

    resetLocalStorage() {
        for (let i = 0; i < localStorage.length; i++) {
            let key = localStorage.key(i);

            if (key.slice(0, 5) === 'book_' ||
                key.slice(0, 12) === 'amount_book_' ||
                key === 'billing_address' ||
                key === 'shipping_address') {
                localStorage.removeItem(key);
            }
        }
    }
}
