import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {HomeComponent} from "./home/home.component";
import {BookListComponent} from "./book/book-list/book-list.component";
import {BookDetailsComponent} from "./book/book-details/book-details.component";
import {BookFormComponent} from "./book/book-form/book-form.component";
import {LoginComponent} from "./login/login.component";
import {OrderAdminComponent} from "./order/order-admin/order-admin.component";
import {OrderListComponent} from "./order/order-list/order-list.component";
import {ShoppingCartComponent} from "./shopping-cart/shopping-cart.component";
import {OrderDetailsComponent} from "./order/order-details/order-details.component";
import {ShoppingCartCheckoutComponent} from "./shopping-cart-checkout/shopping-cart-checkout.component";

const routes: Routes = [
    {path: '', redirectTo: 'home', pathMatch: 'full'},
    {path: 'home', component: HomeComponent},
    {path: 'books', component: BookListComponent},
    {path: 'books/:isbn', component: BookDetailsComponent},
    {path: 'myorders', component: OrderListComponent},
    {path: 'myorder/:id', component: OrderDetailsComponent},
    {path: 'shoppingcart', component: ShoppingCartComponent},
    {path: 'shoppingcart/checkout', component: ShoppingCartCheckoutComponent},
    {path: 'orders', component: OrderAdminComponent},
    {path: 'newbook', component: BookFormComponent},
    {path: 'newbook/:isbn', component: BookFormComponent},
    {path: 'login', component: LoginComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule {

}