import {Component, Input, OnInit, Output} from '@angular/core';
import {Book} from "../../shared/book";

@Component({
    selector: 'a.bs-book-list-item',
    templateUrl: './book-list-item.component.html',
    styles: []
})
export class BookListItemComponent implements OnInit {

    @Input() book: Book;

    constructor() {
    }

    ngOnInit() {
    }

    getRating(num: number) {
        return new Array(num);
    }
}