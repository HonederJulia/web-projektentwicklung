import {ActivatedRoute, Router} from '@angular/router';
import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, FormArray, Validators} from '@angular/forms';
import {BookFormErrorMessages} from './book-form-error-messages';
import {BookFactory} from "../../shared/book-factory";
import {BookService} from "../../shared/book.service";
import {Book, Image} from "../../shared/book";
import {BookValidators} from "../../shared/book-validators";
import {AuthService} from "../../shared/authentication.service";

@Component({
    selector: 'bs-book-form',
    templateUrl: './book-form.component.html'
})
export class BookFormComponent implements OnInit {

    bookForm: FormGroup;
    book = BookFactory.empty();
    authors: FormArray;
    images: FormArray;
    errors: { [key: string]: string } = {};
    isUpdatingBook = false;

    constructor(private fb: FormBuilder,
                private bs: BookService,
                public authService: AuthService,
                private route: ActivatedRoute,
                private router: Router) {
    }

    ngOnInit() {
        const isbn = this.route.snapshot.params['isbn'];

        if (isbn) {
            this.isUpdatingBook = true;
            this.bs.getSingle(isbn).subscribe(book => {
                this.book = book;
                // subscribe is an async method call -> init has to be in this method to display book data
                this.initBook();
            });
        }

        this.initBook();
    }

    initBook() {
        this.buildAuthorsArray();
        this.buildThumbnailsArray();

        this.bookForm = this.fb.group({
            id: this.book.id,
            title: [this.book.title, [
                Validators.required
            ]],
            subtitle: this.book.subtitle,
            authors: this.authors,
            isbn: [this.book.isbn, [
                Validators.required,
                BookValidators.isbnFormat
            ]],
            description: this.book.description,
            rating: [this.book.rating, [
                Validators.min(0),
                Validators.max(10)
            ]],
            price_net: this.book.price_net,
            images: this.images,
            published: new Date(this.book.published)
        });

        this.bookForm.statusChanges.subscribe(() => this.updateErrorMessages());
    }

    buildAuthorsArray() {
        this.authors = this.fb.array(
            this.book.authors.map(
                t => this.fb.group({
                    author_id: this.fb.control(t.id),
                    firstname: this.fb.control(t.firstname),
                    lastname: this.fb.control(t.lastname)
                })
            ),
            BookValidators.atLeastOneAuthor
        );
    }

    addAuthorControl() {
        this.authors.push(this.fb.group({firstname: null, lastname: null}));
    }

    removeAuthorControl(index) {
        if (this.authors.length == 1) {
            this.authors.at(index).reset();
        }
        else
            this.authors.removeAt(index);
    }

    buildThumbnailsArray() {
        this.images = this.fb.array(
            this.book.images.map(
                t => this.fb.group({
                    image_id: this.fb.control(t.id),
                    url: this.fb.control(t.url),
                    title: this.fb.control(t.title)
                })
            ),
            BookValidators.atLeastOneImage
        );
    }

    addThumbnailControl() {
        this.images.push(this.fb.group({url: null, title: null}));
    }

    removeThumbnailControl(index) {
        if (this.images.length == 1) {
            this.images.at(index).reset();
        }
        else
            this.images.removeAt(index);
    }

    submitForm() {
        // filter empty values
        this.bookForm.value.authors = this.bookForm.value.authors.filter(author => author.firstname);

        // filter empty values
        this.bookForm.value.images = this.bookForm.value.images.filter(thumbnail => thumbnail.url);

        const book: Book = BookFactory.fromObject(this.bookForm.value);

        // deep copy  - did not work without
        book.authors = this.bookForm.value.authors;
        book.images = this.bookForm.value.images;
        book.deleted = 0;

        if (this.isUpdatingBook) {
            this.bs.update(book).subscribe(res => {
                this.router.navigate(['../../books', book.isbn], {relativeTo: this.route});
            });
        } else {
            // + operator - converts string to number
            book.user_id = +localStorage.getItem('userId');

            this.bs.save(book).subscribe(res => {
                this.router.navigate(['../books'], {relativeTo: this.route});
            });
        }
    }

    updateErrorMessages() {
        this.errors = {};
        for (const message of BookFormErrorMessages) {
            const control = this.bookForm.get(message.forControl);
            if (control &&
                control.dirty &&
                control.invalid &&
                control.errors[message.forValidator] &&
                !this.errors[message.forControl]) {
                this.errors[message.forControl] = message.text;
            }
        }
    }
}