import {Component, OnInit} from '@angular/core';
import {Order} from "../../shared/order";
import {OrderService} from "../../shared/order.service";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../../shared/authentication.service";
import {OrderFactory} from "../../shared/order-factory";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {OrderDetailsErrorMessages} from "./order-details-error-messages";
import {Status} from "../../shared/status";
import {StatusFactory} from "../../shared/status-factory";

@Component({
    selector: 'bs-order-details',
    templateUrl: './order-details.component.html',
    styles: []
})
export class OrderDetailsComponent implements OnInit {

    newStatusForm: FormGroup;
    order: Order = OrderFactory.empty();
    errors: { [key: string]: string } = {};
    status: Status = StatusFactory.empty();

    constructor(private fb: FormBuilder,
                private os: OrderService,
                private route: ActivatedRoute,
                public authService: AuthService) {
    }

    ngOnInit() {
        const params = this.route.snapshot.params;
        this.os.getSingle(params['id']).subscribe(res => this.order = res);

        this.initStatus();
    }

    initStatus() {
        this.newStatusForm = this.fb.group({
            status: [this.status.status, [
                Validators.required
            ]],
            comment: this.status.comment
        });

        this.newStatusForm.statusChanges.subscribe(() => this.updateErrorMessages());
    }

    submitForm() {
        const params = this.route.snapshot.params;
        const status: Status = StatusFactory.fromObject(this.newStatusForm.value);

        status.order_id = params['id'];

        this.os.saveStatus(status).subscribe(res => {
            this.ngOnInit();
        });
    }

    updateErrorMessages() {
        this.errors = {};
        for (const message of OrderDetailsErrorMessages) {
            const control = this.newStatusForm.get(message.forControl);
            if (control &&
                control.dirty &&
                control.invalid &&
                control.errors[message.forValidator] &&
                !this.errors[message.forControl]) {
                this.errors[message.forControl] = message.text;
            }
        }
    }
}