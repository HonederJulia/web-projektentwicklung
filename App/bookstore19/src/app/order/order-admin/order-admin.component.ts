import {Component, OnInit} from '@angular/core';
import {OrderService} from "../../shared/order.service";
import {Order} from "../../shared/order";
import {$} from "protractor";
import {AuthService} from "../../shared/authentication.service";

@Component({
    selector: 'bs-order-admin',
    templateUrl: './order-admin.component.html',
    styles: []
})
export class OrderAdminComponent implements OnInit {

    orders: Order[];

    constructor(private os: OrderService,
                public authService: AuthService) {
    }

    ngOnInit() {
        this.os.getAll().subscribe(res => this.orders = res);
    }
}
