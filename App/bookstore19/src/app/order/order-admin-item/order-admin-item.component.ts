import {Component, Input, OnInit} from '@angular/core';
import {Order} from "../../shared/order";

@Component({
    selector: 'a.bs-order-admin-item',
    templateUrl: './order-admin-item.component.html',
    styles: []
})
export class OrderAdminItemComponent implements OnInit {

    @Input() order: Order;

    constructor() {
    }

    ngOnInit() {
    }
}