import {Component} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Book} from "../shared/book";
import {BookService} from "../shared/book.service";

@Component({
    selector: 'bs-home',
    templateUrl: './home.component.html',
    styles: []
})
export class HomeComponent {

    books: Book[];

    constructor(private bs: BookService,
                private router: Router,
                private route: ActivatedRoute) {

        this.bs.getNewReleases(4).subscribe(res => this.books = res);
    }

    bookSelected(book: Book) {
        this.router.navigate(['../books', book.isbn], {relativeTo: this.route});
    }
}