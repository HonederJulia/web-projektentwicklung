<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Book extends Model
{
    protected $fillable = [
        'isbn', 'title', 'subtitle', 'author', 'description', 'published', 'rating', 'price_net', 'user_id', 'deleted'
    ];

    // QueryScopes
    public function scopeFavourite($query)
    {
        return $query->where('rating', '>=', 8);
    }

    public function images(): HasMany
    {
        return $this->hasMany(Image::class);
    }

    public function user() : BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function authors(): BelongsToMany
    {
        return $this->belongsToMany(Author::class)->withTimestamps();
    }

    public function orders(): BelongsToMany
    {
        return $this->belongsToMany(Order::class)->withPivot('price_net', 'amount');
    }
}
