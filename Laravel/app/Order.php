<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Order extends Model
{
    protected $fillable = [
        'user_id', 'billing_address_id', 'shipping_address_id', 'date', 'price_total_net', 'price_total_gross'
    ];

    // orders belongs to many books
    public function books(): BelongsToMany
    {
        return $this->belongsToMany(Book::class)->withTimestamps()->withPivot('price_net', 'amount');
    }

    public function statuses(): HasMany
    {
        return $this->hasMany(Status::class);
    }

    public function user() : BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function billingAddress() : BelongsTo
    {
        return $this->belongsTo(Address::class);
    }

    public function shippingAddress() : BelongsTo
    {
        return $this->belongsTo(Address::class);
    }
}
