<?php

namespace App\Http\Controllers;

use App\Order;
use App\Status;
use DateTimeZone;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public function index()
    {
        // load all orders and relations with eager loading
        $orders = Order::with(['books', 'books.images', 'books.authors', 'user', 'statuses'])
            ->orderBy('user_id')
            ->orderBy('date', 'desc')
            ->get();
        return $orders;
    }

    public function ordersByUserId(int $user_id)
    {
        // load all orders and relations with eager loading
        $orders = Order::where('user_id', $user_id)
            ->with(['books', 'user', 'billingAddress', 'shippingAddress', 'statuses'])
            ->orderBy('date', 'desc')
            ->get();
        return $orders;
    }

    public function orderById(string $id)
    {
        $order = Order::where('id', $id)
            ->with(['books', 'books.images', 'books.authors', 'user', 'billingAddress', 'shippingAddress', 'statuses'])
            ->first();
        return $order;
    }

    /**
     * save new order
     */
    public function saveOrder(Request $request): JsonResponse
    {
        $request = $this->parseRequest($request);

        // use a transaction for saving model including relations
        // if one query fails, complete SQL statements will be rolled back
        DB::beginTransaction();

        try {
            $order = Order::create($request->all());

            // save books
            if (isset($request['books']) && is_array($request['books'])) {
                foreach ($request['books'] as $b) {
                    $order->books()->attach($b['id'], ['price_net' => $b['price_net'], 'amount' => $b['amount']]);
                }
            }

            // save status
            $status = Status::firstOrNew(['date' => new \DateTime(), 'status' => 'open']);
            $order->statuses()->save($status);

            DB::commit();

            // return a vaild http response
            return response()->json($order, 201);

        } catch (\Exception $e) {
            // rollback all queries
            DB::rollBack();
            return response()->json("saving order failed: " . $e->getMessage(), 420);
        }
    }

    /**
     * save new order
     */
    public function saveStatus(Request $request): JsonResponse
    {
        $request = $this->parseRequest($request);

        // use a transaction for saving model including relations
        // if one query fails, complete SQL statements will be rolled back
        DB::beginTransaction();

        try {
            $status = Status::create($request->all());

            DB::commit();

            // return a vaild http response
            return response()->json($status, 201);

        } catch (\Exception $e) {
            // rollback all queries
            DB::rollBack();
            return response()->json("saving status failed: " . $e->getMessage(), 420);
        }
    }

    /**
     * modify / convert values if needed
     */
    private function parseRequest(Request $request): Request
    {
        // get date and convert it - its in ISO 8601, e.g. "2018-01-01T23:00:00.000Z"
        $date = new \DateTime($request->date);
        $request['date'] = $date;
        return $request;
    }
}