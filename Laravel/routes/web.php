<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now save something great!
|
*/

use Illuminate\Support\Facades;

Route::get('/', 'BookController@index');

Route::get('/books', 'BookController@index');

Route::get('books/{id}', 'BookController@show');

/*
Route::get('/', function () {
    $books = DB::table('books')->get();

    //DB::select('select * from books');
    //DB::table('books')->where('title', 'Harry Potter')->get();
    //return $books;
    return view('welcome', compact('books'));
});

Route::get('books', function () {
    //$books = DB::table('books')->get();
    $books = Book::all();
    return view('books.index', compact('books'));
});

Route::get('books/{id}', function ($id) {
    //$book = DB::table('books')->find($id);
    $book = Book::find($id);
    return view('books.show', compact('book'));
});
*/