<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>KWM</title>

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Calibri', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }

        .book-title {
            text-align: center;
        }

        .row {
            display: flex;
        }

        [class^="col-"] {
            padding: 0 15px;
        }

        .col-12 {
            width: 100%;
        }

        .col-6 {
            width: 50%;
        }

        .col-5 {
            width: 41.66%;
        }

        .col-4 {
            width: 33.33%;
        }

        .col-2 {
            width: 16.66%;
        }

        .col-1 {
            width: 8.33%;
        }

        .line-header {
            margin-bottom: 10px;
            padding-bottom: 5px;
            border-bottom: 1px solid #636b6f;
        }

        .bold {
            font-weight: bold;
        }

        label {
            display: inline-block;
            min-width: 150px;
        }
    </style>
</head>
<body>
    <div class="row">
        <div class="col-12">
            <h1 class="book-title">{{$book->title}}</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-1"></div>
        <div class="col-2">
            <img src="{{$book->images}}" />
        </div>
        <div class="col-4">
            <p class="line-header"><span class="bold">Details</span></p>
            <p>
                <label>ISBN:</label>{{$book->isbn}}<br/>
                <label>Author:</label>{{$book->author}}<br/>
                <label>Erscheinungsdatum:</label>{{$book->published}}
            </p>
        </div>
        <div class="col-4">
            <p class="line-header"><span class="bold">Beschreibung</span></p>
            <p>
                {{$book->description}}
            </p>
        </div>
        <div class="col-1"></div>
    </div>
</body>
</html>