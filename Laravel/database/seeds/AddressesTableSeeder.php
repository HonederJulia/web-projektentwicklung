<?php

use App\Address;
use App\User;
use Illuminate\Database\Seeder;

class AddressesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $address1 = new Address();
        $address1->firstname = 'Julia';
        $address1->lastname = 'Honeder';
        $address1->street = 'Softwarepark';
        $address1->number = '31c/5';
        $address1->zip = '4232';
        $address1->city = 'Hagenberg im Mühlkreis';
        $address1->country = 'Austria';

        $user1 = User::all()->first();
        $address1->user()->associate($user1);

        $address1->save();

        // -------------------------------------------------------------------------------

        $address2 = new Address();
        $address2->firstname = 'Sarah';
        $address2->lastname = 'Honeder';
        $address2->street = 'Kirchschlag';
        $address2->number = '58';
        $address2->zip = '3631';
        $address2->city = 'Kirchschlag';
        $address2->country = 'Austria';

        $user2 = User::all()->first();
        $address2->user()->associate($user2);

        $address2->save();

        // -------------------------------------------------------------------------------

        $address3 = new Address();
        $address3->firstname = 'Philipp';
        $address3->lastname = 'Preiser';
        $address3->street = 'Pletzensieldung';
        $address3->number = '317';
        $address3->zip = '3920';
        $address3->city = 'Groß Gerungs';
        $address3->country = 'Austria';

        $user3 = User::all()->get('id', 2);
        $address3->user()->associate($user3);

        $address3->save();
    }
}
