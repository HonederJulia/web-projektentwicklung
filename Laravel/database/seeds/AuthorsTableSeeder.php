<?php

use App\Author;
use Illuminate\Database\Seeder;

class AuthorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $author1 = new Author();
        $author1->firstName = 'Joanne K.';
        $author1->lastName = 'Rowling';
        $author1->save();

        // -------------------------------------------------------------------------------

        $author2 = new Author();
        $author2->firstName = 'J. R. R.';
        $author2->lastName = 'Tolkien';
        $author2->save();

        // -------------------------------------------------------------------------------

        $author3 = new Author();
        $author3->firstName = 'George R. R.';
        $author3->lastName = 'Martin';
        $author3->save();
    }
}